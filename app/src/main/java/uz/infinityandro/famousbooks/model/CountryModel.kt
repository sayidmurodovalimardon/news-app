package uz.infinityandro.famousbooks.model

data  class CountryModel(
    var image:Int,
    var nameCountry:String,
    var code:String
)
