package uz.infinityandro.famousbooks.model

data class CategoryModel(
    var image:Int,
    var name:String
)