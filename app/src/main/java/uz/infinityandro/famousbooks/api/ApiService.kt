package uz.infinityandro.famousbooks.api

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import uz.infinityandro.famousbooks.model.Info

interface ApiService {
    @GET("v2/top-headlines")
    suspend fun getAllBooks(@Query("country") country:String,
                            @Query("page") pageNumber:Int,
                            @Query("category") category:String,
                            @Query("apiKey") key:String
    ):Response<Info>

}