package uz.infinityandro.famousbooks.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import uz.infinityandro.famousbooks.util.Constants

object ApiClient {
    var retrofit:Retrofit?=null

    fun getBooks():Retrofit{
        retrofit=Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constants.BASE_URL)
            .build()

        return retrofit!!
    }
}