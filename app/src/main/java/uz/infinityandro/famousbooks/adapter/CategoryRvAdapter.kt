package uz.infinityandro.famousbooks.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import uz.infinityandro.famousbooks.R
import uz.infinityandro.famousbooks.databinding.CategoryRvItemBinding
import uz.infinityandro.famousbooks.model.CategoryModel

class CategoryRvAdapter(
    var list: List<CategoryModel>,
    var itemPosition:Int,
    var listener: (model: CategoryModel) -> Unit

) : RecyclerView.Adapter<CategoryRvAdapter.VH>() {

    inner class VH(var binding: CategoryRvItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(categoryModel: CategoryModel, position: Int) {

            binding.root.setOnClickListener {
                itemPosition = adapterPosition
                listener(categoryModel)
                notifyDataSetChanged()
            }
            binding.categoryImage.setImageResource(categoryModel.image)
            binding.categoryText.text = categoryModel.name

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(CategoryRvItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position], position)
        if (position == itemPosition) {
            holder.binding.card.elevation = 10f
            holder.binding.card.alpha=1f
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                holder.binding.card.outlineAmbientShadowColor=ContextCompat.getColor(holder.binding.root.context,
                    R.color.white)
                holder.binding.card.outlineSpotShadowColor=ContextCompat.getColor(holder.binding.root.context,
                    R.color.white)
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {

            }
            holder.binding.card.alpha=0.3f
            holder.binding.card.elevation = 1f

        }


    }

    override fun getItemCount(): Int = list.size
}