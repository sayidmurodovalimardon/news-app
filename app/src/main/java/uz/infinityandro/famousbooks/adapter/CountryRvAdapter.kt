package uz.infinityandro.famousbooks.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.infinityandro.famousbooks.databinding.CountryRvItemBinding
import uz.infinityandro.famousbooks.model.CountryModel

class CountryRvAdapter(var list: List<CountryModel>,var listener:(model:CountryModel)->Unit):RecyclerView.Adapter<CountryRvAdapter.VH>() {
    var itemPosition = 0

    inner class VH(var binding: CountryRvItemBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(countryModel: CountryModel) {
            binding.root.setOnClickListener {
                itemPosition=adapterPosition
                listener(countryModel)
                notifyDataSetChanged()
            }
            binding.country.setImageResource(countryModel.image)
            binding.textCountry.text=countryModel.nameCountry
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(CountryRvItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
        if (position==itemPosition){
            holder.binding.textCountry.alpha=1f
            holder.binding.country.alpha=1f

        }else{
            holder.binding.textCountry.alpha=0.3f
            holder.binding.country.alpha=0.3f

        }
    }

    override fun getItemCount(): Int =list.size
}