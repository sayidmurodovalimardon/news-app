package uz.infinityandro.famousbooks.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import uz.infinityandro.famousbooks.R
import uz.infinityandro.famousbooks.databinding.NewsRvItemBinding
import uz.infinityandro.famousbooks.model.ArticlesItem

class NewsRvAdapter (var context:Context,var list: List<ArticlesItem>,var listener:(model:ArticlesItem)->Unit):RecyclerView.Adapter<NewsRvAdapter.VH>(){
    inner class VH(var binding: NewsRvItemBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(articlesItem: ArticlesItem) {
            binding.root.setOnClickListener {
                listener(articlesItem)
            }
            Glide.with(context).load(articlesItem.urlToImage).into(binding.newsImage)
            binding.newsTitle.setText(articlesItem.title)
            binding.newsText.setText(articlesItem.description)
            binding.cardView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.recycler_view_anim_bottom))
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(NewsRvItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])

    }

    override fun getItemCount(): Int =list.size
}