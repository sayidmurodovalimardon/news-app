package uz.infinityandro.famousbooks.activity

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import com.bumptech.glide.Glide
import uz.infinityandro.famousbooks.R
import uz.infinityandro.famousbooks.databinding.ActivityNewsBinding
import uz.infinityandro.famousbooks.model.ArticlesItem

class NewsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNewsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityNewsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        onBack()
        getNewsDetails()
    }

    private fun getNewsDetails() {
        val image=intent.getStringExtra("image")
        val title=intent.getStringExtra("title")
        val website=intent.getStringExtra("website")
        val content=intent.getStringExtra("content")
        val date=intent.getStringExtra("date")
        binding.website.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(website)
            startActivity(intent)
        }

        Glide.with(this).load(image).into(binding.newsImage)
        binding.date.setText("Published At: $date")
        binding.content.text = content.toString()
        binding.title.text=title.toString()
        binding.read.setOnClickListener {
            if (binding.read.text.toString().equals("Read More")){
                binding.content.maxLines= Int.MAX_VALUE
                binding.content.ellipsize=null
                binding.read.text="Read Less"
            }else{
                binding.content.maxLines= 4
                binding.content.ellipsize= TextUtils.TruncateAt.END
                binding.read.text="Read More"
            }
        }
    }

    private fun onBack() {
        binding.back.setOnClickListener {
            onBackPressed()
        }
    }
}