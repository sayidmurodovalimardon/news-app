package uz.infinityandro.famousbooks.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ScrollView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.famousbooks.R
import uz.infinityandro.famousbooks.adapter.CategoryRvAdapter
import uz.infinityandro.famousbooks.adapter.CountryRvAdapter
import uz.infinityandro.famousbooks.adapter.NewsRvAdapter
import uz.infinityandro.famousbooks.databinding.ActivityMainBinding
import uz.infinityandro.famousbooks.model.ArticlesItem
import uz.infinityandro.famousbooks.util.Constants
import uz.infinityandro.famousbooks.viewModel.Impl.BookViewModelImpl

class MainActivity : AppCompatActivity() {
    private val viewModelImpl: BookViewModelImpl by viewModel()
    private lateinit var categoryRvAdapter: CategoryRvAdapter
    private lateinit var countryRvAdapter: CountryRvAdapter
    private lateinit var newsRvAdapter: NewsRvAdapter
    private lateinit var binding: ActivityMainBinding
    var list = ArrayList<ArticlesItem>()
    var page = 1
    var country = "us"
    var category = "business"
    var positionAdapter = 0
    var totalPages = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        adapters()
        viewModelss()
        listener()
        books()

    }

    private fun listener() {
        binding.retry.setOnClickListener {
            books()
        }
    }



    private fun books() {
        viewModelImpl.getAllBooks(country, page, category)
    }

    private fun viewModelss() {
        viewModelImpl.connectionLiveData.observe(this, {
            if (it) {
                binding.header.visibility=View.GONE
                binding.network.visibility=View.VISIBLE
            }else{
                binding.header.visibility=View.VISIBLE
                binding.network.visibility=View.GONE
            }
        })
        viewModelImpl.allDataLiveData.observe(this, {
            if (!it.articles.isNullOrEmpty()) {
                list.addAll(it.articles!!)
                totalPages = it.totalResults!!
                newsRvAdapter.notifyDataSetChanged()
            }

        })
        viewModelImpl.errorMessageLiveData.observe(this, {
            onSnackError(binding.main)
        })
        viewModelImpl.progressLiveData.observe(this) {
            binding.progress.visibility = if (it) View.VISIBLE else View.GONE
        }
    }

    @SuppressLint("ResourceAsColor")
    private fun onSnackError(main: ScrollView) {
        val snackBar = Snackbar.make(main, "Error", Snackbar.LENGTH_SHORT)
            .setAction("Error", null)
        snackBar.setActionTextColor(Color.WHITE)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(R.color.colorPrimaryDark)
        val textView =
            snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.WHITE)
        textView.textSize = 22f
        snackBar.show()
    }

    @SuppressLint("ResourceAsColor")
    private fun onSnack(main: ScrollView) {
        val snackBar = Snackbar.make(main, "No Internet Connection", Snackbar.LENGTH_SHORT)
            .setAction("Internet", null)
        snackBar.setActionTextColor(Color.WHITE)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(R.color.colorPrimaryDark)
        val textView =
            snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.WHITE)
        textView.textSize = 22f
        snackBar.show()
    }

    private fun adapters() {

        categoryRvAdapter = CategoryRvAdapter(Constants.getCategories(), positionAdapter) {
            list.clear()
            category = it.name
            viewModelImpl.getAllBooksNew(country, page, category)
        }
        binding.categoriesRv.adapter = categoryRvAdapter
        countryRvAdapter = CountryRvAdapter(Constants.getCountries()) {
            list.clear()
            country = it.code
            viewModelImpl.getAllBooksNew(country, page, category)
        }
        binding.countryRv.adapter = countryRvAdapter
        newsRvAdapter = NewsRvAdapter(this, list) { news ->
            var intent = Intent(applicationContext, NewsActivity::class.java)
            intent.putExtra("image", news.urlToImage)
            intent.putExtra("website", news.url)
            intent.putExtra("title", news.title)
            intent.putExtra("content", news.content)
            intent.putExtra("date", news.publishedAt)
            startActivity(intent)
        }
        binding.newsRv.adapter = newsRvAdapter
    }
}