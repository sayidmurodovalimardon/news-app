package uz.infinityandro.famousbooks.dagger

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import uz.infinityandro.famousbooks.viewModel.Impl.BookViewModelImpl

val viewModelModule= module {

    viewModel { BookViewModelImpl(repository = get()) }
}