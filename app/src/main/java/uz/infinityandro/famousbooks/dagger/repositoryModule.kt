package uz.infinityandro.famousbooks.dagger

import org.koin.dsl.module
import uz.infinityandro.famousbooks.repository.BookRepository
import uz.infinityandro.famousbooks.repository.Impl.BookRepositoryImpl

val repositoryModule = module {
    factory<BookRepository> { BookRepositoryImpl() }
}