package uz.infinityandro.famousbooks.repository.Impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import uz.infinityandro.famousbooks.api.ApiClient
import uz.infinityandro.famousbooks.api.ApiService
import uz.infinityandro.famousbooks.model.Info
import uz.infinityandro.famousbooks.repository.BookRepository

class BookRepositoryImpl : BookRepository {
    override fun getAllBooks(country:String,page:Int,category:String,key: String): Flow<Result<Info>> = flow {
        val api = ApiClient.getBooks().create(ApiService::class.java)
        val response = api.getAllBooks(country,page,category,key)
        if (response.isSuccessful) {
            emit(Result.success(response.body()!!))
        } else {
            val message = "Error"
            emit(Result.failure(Throwable(message)))
        }

    }.flowOn(Dispatchers.Main)
}