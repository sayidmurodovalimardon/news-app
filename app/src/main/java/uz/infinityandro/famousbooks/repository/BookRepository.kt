package uz.infinityandro.famousbooks.repository

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.famousbooks.model.Info

interface BookRepository {
    fun getAllBooks(country:String,page:Int,category:String,key:String):Flow<Result<Info>>
}