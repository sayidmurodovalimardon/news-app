package uz.infinityandro.famousbooks.util

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import uz.infinityandro.famousbooks.dagger.repositoryModule
import uz.infinityandro.famousbooks.dagger.viewModelModule

@Suppress("unused")
class AppConfig: Application() {
    companion object {
        lateinit var instance: AppConfig
            private set
    }
    override fun onCreate() {
        super.onCreate()
        instance=this
        startKoin {
            androidContext(this@AppConfig)
            modules(listOf(repositoryModule, viewModelModule))
        }

    }
}