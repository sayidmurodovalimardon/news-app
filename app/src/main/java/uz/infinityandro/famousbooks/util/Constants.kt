package uz.infinityandro.famousbooks.util

import uz.infinityandro.famousbooks.R
import uz.infinityandro.famousbooks.model.CategoryModel
import uz.infinityandro.famousbooks.model.CountryModel

object Constants {
//    const val ACCESS_KEY="ae5b17ed05054689b578c815e9600bdd"
    const val ACCESS_KEY="12569ee8a1ca4606ada91b01ab4d96bf"
    const val BASE_URL="https://newsapi.org"

    fun getCountries():ArrayList<CountryModel>{
        val list=ArrayList<CountryModel>()
        list.add(CountryModel(R.drawable.us,"US","us"))
        list.add(CountryModel(R.drawable.ae_arab,"Arab Emirates","ae"))
        list.add(CountryModel(R.drawable.ar_argentina,"Argentina","ar"))
        list.add(CountryModel(R.drawable.ua_ukraina,"Ukraine","ua"))
        list.add(CountryModel(R.drawable.nz_new_zeland,"New Zealand","nz"))
        list.add(CountryModel(R.drawable.ph_philipine,"Philippines","ph"))
        list.add(CountryModel(R.drawable.sa_south_sfrica,"South Africa","sa"))
        list.add(CountryModel(R.drawable.se_sweden,"Sweden","se"))
        list.add(CountryModel(R.drawable.sg_singapore,"Singapore","sg"))
        list.add(CountryModel(R.drawable.si_slovenia,"Slovenia","si"))
        list.add(CountryModel(R.drawable.bg_bulgaria,"Bulgaria","bg"))
        list.add(CountryModel(R.drawable.fr_france,"France","fr"))
        list.add(CountryModel(R.drawable.gb_united_kingdom,"UK","gb"))
        list.add(CountryModel(R.drawable.gr_greece,"Greece","gr"))
        list.add(CountryModel(R.drawable.ng_nigeria,"Nigeria","ng"))
        list.add(CountryModel(R.drawable.th_thailand,"Thailand","th"))
        list.add(CountryModel(R.drawable.tr_turkey,"Turkey","tr"))
        list.add(CountryModel(R.drawable.tw_taiwan,"Taiwan","tw"))
        return list
    }

    fun getCategories():ArrayList<CategoryModel>{
        val list=ArrayList<CategoryModel>()
        list.add(CategoryModel(R.drawable.business,"business"))
        list.add(CategoryModel(R.drawable.entertainment,"entertainment"))
        list.add(CategoryModel(R.drawable.general,"general"))
        list.add(CategoryModel(R.drawable.health,"health"))
        list.add(CategoryModel(R.drawable.science,"science"))
        list.add(CategoryModel(R.drawable.sport,"sports"))
        list.add(CategoryModel(R.drawable.texnologia,"technology"))
        return list
    }
}