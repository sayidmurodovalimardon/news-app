package uz.infinityandro.famousbooks.viewModel

import androidx.lifecycle.LiveData
import uz.infinityandro.famousbooks.model.Info

interface BookviewModel {
    val errorMessageLiveData: LiveData<String>
    val connectionLiveData: LiveData<Boolean>
    val allDataLiveData: LiveData<Info>
    val progressLiveData:LiveData<Boolean>

    fun getAllBooks(country:String,page:Int,category:String)
    fun getAllBooksNew(country:String,page:Int,category:String)

}