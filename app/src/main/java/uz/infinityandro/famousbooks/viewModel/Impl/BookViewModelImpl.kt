package uz.infinityandro.famousbooks.viewModel.Impl

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.famousbooks.model.Info
import uz.infinityandro.famousbooks.network.NetworkHelper
import uz.infinityandro.famousbooks.network.isConnected
import uz.infinityandro.famousbooks.repository.BookRepository
import uz.infinityandro.famousbooks.util.Constants
import uz.infinityandro.famousbooks.viewModel.BookviewModel

class BookViewModelImpl(private val repository: BookRepository
) : ViewModel(), BookviewModel {
    override val progressLiveData= MutableLiveData<Boolean>()
    override val errorMessageLiveData = MutableLiveData<String>()
    override val connectionLiveData = MutableLiveData<Boolean>()
    override val allDataLiveData = MutableLiveData<Info>()

    override fun getAllBooks(country:String,page:Int,category:String) {
        if (!isConnected()){
            connectionLiveData.postValue(true)
            return
        }
        connectionLiveData.postValue(false)
        repository.getAllBooks(country,page,category,Constants.ACCESS_KEY).onEach {
            progressLiveData.postValue(true)

            it.onSuccess { result->
                progressLiveData.postValue(false)
                allDataLiveData.postValue(result)
            }.onFailure { error->
                errorMessageLiveData.postValue(error.toString())
            }
        }.launchIn(viewModelScope)
    }

    override fun getAllBooksNew(country: String, page: Int, category: String) {
        if (!isConnected()){
            connectionLiveData.postValue(true)
            return
        }
        progressLiveData.postValue(true)
        connectionLiveData.postValue(false)
        repository.getAllBooks(country, page, category, Constants.ACCESS_KEY).onEach {
                it.onSuccess { result ->
                    progressLiveData.postValue(false)
                    allDataLiveData.postValue(result)
                }.onFailure { error ->
                    errorMessageLiveData.postValue(error.toString())
                }
            }.launchIn(viewModelScope)
    }



}